<?php
/*
 * PIP v0.5.3
 */

//Start the Session
session_start(); 

//Settings
ini_set('default_socket_timeout', 1); //in seconds

// Defines
define('ROOT_DIR', realpath(dirname(__FILE__)) .'/');
define('APP_DIR', ROOT_DIR .'application/');
define('HOSTNAME', 'mc.happy-hg.com');
define('PORT', 25565);

// Includes
require(APP_DIR .'config/config.php');
require(ROOT_DIR .'system/model.php');
require(ROOT_DIR .'system/view.php');
require(ROOT_DIR .'system/controller.php');
require(ROOT_DIR .'system/pip.php');

// Define base URL
global $config;
define('BASE_URL', $config['base_url']);

pip();

?>
