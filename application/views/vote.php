<?php $i = 4; include('header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6" id="announcement-feed">
                <h2 class="content-lead">Top 10 Voters | <?=$totalVotes;?> total votes</h2>
                <?php
                    $i = 1;
                    foreach($topVoters as $v){
                        echo '
                        <div class="announcement">
                            <div class="row">
                                <div class="col-md-1">
                                    <img class="panel-avatar" src="https://minotar.net/helm/' . $v->username . '/50.png">
                                </div>
                                
                                <div class="col-md-11">
                                    <div class="panel panel-default">
                                        <div class="panel-body announcement-body">
                                            <h4>#' . $i . ' ' . $v->username . ' > ' . $v->votes . ' votes</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        ';
                        $i++;
                    }
                ?>
            </div>
            <div class="col-md-6" id="twitter-feed">
                <h2 class="content-lead">Recently Voted</h2>
                <?php
                    foreach($recentVoters as $v){
                        echo '
                        <div class="announcement">
                            <div class="row">
                                <div class="col-md-1">
                                    <img class="panel-avatar" src="https://minotar.net/helm/' . $v->username . '/50.png">
                                </div>
                                
                                <div class="col-md-11">
                                    <div class="panel panel-default">
                                        <div class="panel-body announcement-body">
                                            <h4>' . $v->username . ' > ' . $v->votes . ' votes</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        ';
                    }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs vote-tabs" id="tabList">
                    <li role="presentation" class="active"><a href="#1" aria-controls="1">Vote #1</a></li>
                    <li role="presentation"><a href="#2" aria-controls="2">Vote #2</a></li>
                    <li role="presentation"><a href="#3" aria-controls="3">Vote #3</a></li>
                    <li role="presentation"><a href="#4" aria-controls="4">Vote #4</a></li>
                    <li role="presentation"><a href="#5" aria-controls="5">Vote #5</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="1"><iframe src="http://www.ozairpatel.com/" width="100%" height="750px"></iframe></div>
                    <div role="tabpanel" class="tab-pane" id="2"><iframe src="http://www.ozairpatel.com/" width="100%" height="750px"></iframe></div>
                    <div role="tabpanel" class="tab-pane" id="3"><iframe src="http://www.ozairpatel.com/" width="100%" height="750px"></iframe></div>
                    <div role="tabpanel" class="tab-pane" id="4"><iframe src="http://www.ozairpatel.com/" width="100%" height="750px"></iframe></div>
                    <div role="tabpanel" class="tab-pane" id="5"><iframe src="http://www.ozairpatel.com/" width="100%" height="750px"></iframe></div>
                </div>
            </div>
        </div>
    </div>
    
    <?php $scripts = 
    "
    $('#tabList a').click(function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
    ";
    ?>
    <?php include('footer.php'); ?>