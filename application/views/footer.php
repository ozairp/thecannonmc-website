    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=BASE_URL;?>static/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=BASE_URL;?>static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=BASE_URL;?>static/js/app.js"></script>
    <?php if(!isset($include)){ $include = []; } ?>
    <?php foreach($include as $i){
        echo '<script type="text/javascript" src="' . BASE_URL . 'static/' . $i . '"></script>';
    }
    ?>
    <script type="text/javascript">
        <?php
        if(isset($scripts)){
            echo $scripts;
        }
        ?>
        console.log("Written by Ozair/Explodified | All rights belong to Mason Stevens/TheCannonMC");
        var launchEvent = function(loc){
            window.location.href = loc;
        };
    </script>
  </body>
</html>