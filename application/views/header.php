<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TheCannonMC | <?=ucfirst(basename($_SERVER['REQUEST_URI'], ".php"));?></title>

    <link href="<?=BASE_URL;?>static/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=BASE_URL;?>static/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=BASE_URL;?>static/css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <?php if(!isset($bodyClass)){ $bodyClass = ""; } ?>
  <body class="<?=$bodyClass;?>" id="body">
    <?php if(!isset($nav)){ $nav = true; } ?>
    <?php if($nav == true){ ?>
    <nav class="navbar navbar-inverse">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=BASE_URL;?>">
            <!--<img alt="The Cannon MC" id="brandImage" src="<?=BASE_URL;?>static/img/pirate2.png">-->
            TheCannonMC
          </a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li <?php if($i == 0){ echo "class=\"active\""; } ?>><a href="<?=BASE_URL;?>home">Home</a></li>
            <li <?php if($i == 1){ echo "class=\"active\""; } ?>><a target="_blank" href="http://thecannonmc.buycraft.net/">Store</a></li>
            <li <?php if($i == 2){ echo "class=\"active\""; } ?>><a href="/community">Community</a></li>
            <li <?php if($i == 3){ echo "class=\"active\""; } ?>><a href="<?=BASE_URL;?>staff">Staff</a></li>
            <li <?php if($i == 4){ echo "class=\"active\""; } ?>><a href="<?=BASE_URL;?>vote">Vote</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li><span class="navbar-text" id="playerOnline"><?php $r = json_decode(file_get_contents("http://craftapi.com/api/server/info/" . HOSTNAME . ":" . PORT)); if(isset($r->error)){ echo '<strong class="text-danger">OFFLINE</strong>';}else{ echo '<strong class="text-success">' . $r->players->online . '</strong> players online!';} ?></span></li>
              <li><a href="#" id="ip-tooltip" data-toggle="tooltip" data-placement="bottom" title="Click to copy our IP!" onClick="copy('<?=HOSTNAME;?>');"><?=HOSTNAME;?></a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
    <?php } ?>