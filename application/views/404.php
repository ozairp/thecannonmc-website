<?php $i = -1; include('header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Uh oh! <small>404 Error - Not found</small></h2>
                <p>Looks like you took a wrong turn there bud! Try going <a href="<?=BASE_URL;?>">home</a>?</p>
            </div>
        </div>
    </div>
<?php include('footer.php'); ?>