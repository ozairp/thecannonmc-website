<?php $i = 0; include('header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6" id="announcement-feed">
                <h2 class="content-lead">Announcements&nbsp;<small class="pull-right"><a href="#"><i class="fa fa-external-link"></i></a></small></h2>
                <?php
                foreach($announcementPosts as $ap){
                    echo '
              <div class="announcement" onClick="launchEvent(\'\')">
                <div class="row">
                  <div class="col-md-1">
                    <img class="panel-avatar" src="https://minotar.net/helm/' . $ap->username . '/50.png">
                  </div>
                  
                  <div class="col-md-11">
                    <div class="panel panel-default">
                      <div class="panel-body announcement-body">
                        <h4>' . $ap->title . '</h4>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                    ';
                }
                ?>
              
            </div>
            <div class="col-md-6" id="twitter-feed">
                <h2 class="content-lead">Twitter Feed</h2>
                
                <div class="twitter-feed">
              		<a href="https://twitter.com/thecannonmc" class="first"> 
              			@TheCannonMC
              		</a>
              		<ul class="timeline" id="tweets">
              			<h3 class="action center"><i class="fa fa-cog fa-spin"></i> Getting the latest data for you...</h3>
              			
              		</ul>
              	</div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h2 class="content-lead">Top Voters | <?=$totalVotes;?> Total Votes</h2>
                <?php
                $i = 1;
                foreach($topVoters as $v){
                    echo '
              <div class="announcement">
                <div class="row">
                  <div class="col-md-1">
                    <img class="panel-avatar" src="https://minotar.net/helm/' . $v->username . '/50.png">
                  </div>
                  
                  <div class="col-md-11">
                    <div class="panel panel-default">
                      <div class="panel-body announcement-body">
                        <h4>#' . $i. ' ' . $v->username . ' > ' . $v->votes . ' votes</h4>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                    ';
                    $i++;
                }
                ?>
            </div>
        </div>
    </div>
    <?php $scripts = '
    $.get("' . BASE_URL . 'home/twitter", function(data){
      $("#tweets").html(data);
    });
    '; ?>
    <?php include('footer.php'); ?>