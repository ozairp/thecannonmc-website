<?php 

$config['base_url'] = 'http://thecannonmc-ozairp.c9.io/'; // Base URL including trailing slash (e.g. http://localhost/)

$config['default_controller'] = 'main'; // Default controller to load
$config['error_controller'] = 'error'; // Controller used for errors (e.g. 404, 500 etc)

$config['db_host'] = getenv('IP');; // Database host (e.g. localhost)
$config['db_name'] = 'c9'; // Database name
$config['db_username'] = getenv('C9_USER'); // Database username
$config['db_password'] = ''; // Database password

?>