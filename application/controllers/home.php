<?php

class Home extends Controller {
	
	function index()
	{
		$template = $this->loadView('home');
		
		$voteModel = $this->loadModel('vote_model');
    	$topVoters = $voteModel->getTopVoters(5);
    	foreach($topVoters as &$v){
    		if($v->username == null){
    			//TODO: Add CRON job to delete all previous entries.
    			$username = json_decode(file_get_contents('http://mcuuid.com/api/' . $v->uuid), TRUE)['name'];
    			$voteModel->setUserName($v->id, $username);
    			$v->username = $username;
    		}
    	}
    	$template->set('topVoters', $topVoters);
    	$template->set('totalVotes', $voteModel->getTotalVotes());
    	$forumModel = $this->loadModel('forum_model');
    	$template->set('announcementPosts', []/*$forumModel->getLatestPosts(5)*/);
    	
		$template->render();
	}
	
	function twitter(){
		
		$cacheTime = file_get_contents( ROOT_DIR . 'files/twitter.cache.time');
		if(empty($cacheTime)){
			file_put_contents( ROOT_DIR . 'files/twitter.cache.time', time());
			$this->twitter();
			return;
		}else{
			require_once( ROOT_DIR . 'libs/cache.class.php');
			$c = new Cache();
			$c->setCache('tweetCache');
			if( time() - (int)$cacheTime >= 900 || !$c->isCached('tweets')){
				require_once(ROOT_DIR . 'libs/TwitterAPIExchange.php');
				
				$settings = [
					'oauth_access_token' => '1157087821-0svK4smQ2LplZFkMmbwAE2wZbh6tGAnFeT8y5xj',
					'oauth_access_token_secret' => 'olVw1Dw5V9czQr6bu5E8jibYPlNNEu5HrXX5e5SYqhBXw',
					'consumer_key' => 'SXWjKg2CFxXZ5xxsAz1njsagV',
					'consumer_secret' => 'VWxoMQeuyKmew8e1tBkNvjNxobHyGhXJOLF63YSQXTUJhpCfmf',
					];
					
				$url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
				$method = "GET";
					
				$getString = "?screen_name=thecannonmc&count=5";
					
				$twitter = new TwitterAPIExchange($settings);
				$result = json_decode($twitter->setGetfield($getString)
							->buildOauth($url, $method)
							->performRequest(), TRUE);
				
				$output = '';
				foreach($result as $r){
					$output .= '
					<li>
						<div class="avatar">
							<img src="' . str_replace('_normal', '', $result[0]['user']['profile_image_url']) . '">
							<div class="hover">
								<div class="icon-twitter"></div>
							</div>
						</div>
						<div class="bubble-container">
							<div class="bubble">
								<h3>@' . $result[0]['user']['screen_name'] . '</h3><br/>
								' . $r['text'] . '
							</div>
						
							<div class="arrow"></div>
						</div>
					</li>
';
				}
				
				echo $output;
				
				$c->store('tweets', $output);
				file_put_contents( ROOT_DIR . 'files/twitter.cache.time', time());
			}else{
				echo $c->retrieve('tweets');
			}
		}
		
	}
	
	function dumpTwitterCache(){
		file_put_contents( ROOT_DIR . 'files/twitter.cache.time', 0);
		echo "OK";
	}
	
	function dumpUsernameCache(){
		$this->loadModel('vote_model')->dumpUsernameCache();
		echo "OK";
	}
	
	function getPlayerCount(){
		require ROOT_DIR . 'libs/status.class.php';
		$status = new MinecraftServerStatus();
		$r = $status->getStatus(HOSTNAME, PORT);
		if(!$r){
			echo "OFFLINE";
		}else{
			echo $r['players'];
		}
	}
}

?>
