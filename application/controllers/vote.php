<?php

class Vote extends controller{
    
    public function index(){
        $template = $this->loadView('vote');
        
        $voteModel = $this->loadModel('vote_model');
    	$topVoters = $voteModel->getTopVoters(10);
    	foreach($topVoters as &$v){
    		if($v->username == null){
    			//TODO: Add CRON job to delete all previous entries.
    			$username = json_decode(file_get_contents('http://mcuuid.com/api/' . $v->uuid), TRUE)['name'];
    			$voteModel->setUserName($v->id, $username);
    			$v->username = $username;
    		}
    	}
    	$recentVoters = $voteModel->getRecentVoters(10);
    	foreach($recentVoters as &$v){
    		if($v->username == null){
    			//TODO: Add CRON job to delete all previous entries.
    			$username = json_decode(file_get_contents('http://mcuuid.com/api/' . $v->uuid), TRUE)['name'];
    			$voteModel->setUserName($v->id, $username);
    			$v->username = $username;
    		}
    	}
    	$template->set('topVoters', $topVoters);
    	$template->set('recentVoters', $recentVoters);
    	$template->set('totalVotes', $voteModel->getTotalVotes());
    	
        
        $template->render();
    }
    
    public function api($passphrase = ''){
        if($passphrase == 'SkJsnWwM2YZXC6K4nuyTSJHSJC4xYGXGbPfBLU2QmCaQemrx4hTHE9msGC7Dm2kD'){
            $this->{$_GET['method']}($_GET['param']);
        }else{
            http_response_code(403);
            echo "<h1>403 - Forbidden</h1>";
            echo "<p>Your IP has been logged for security purposes</p>";
            echo "<p>" . $_SERVER['HTTP_X_FORWARDED_FOR'] . "</p>";
            file_put_contents(ROOT_DIR . 'files/security.log', $_SERVER['HTTP_X_FORWARDED_FOR'] . "\n", FILE_APPEND);
        }
    }
    
    private function incrementUser($uuid){
        $this->loadModel('vote_model')->incrementUser($this->stripDashes($uuid));
    }
    
    private function incrementUserByUsername($username){
        $this->loadModel('vote_model')->incrementUser($this->stripDashes(json_decode(file_get_contents('http://mcuuid.com/api/' . $username), TRUE)['uuid']));
    }
    
    private function stripDashes($str){
        return str_replace("-", "", $str);
    }
    
}