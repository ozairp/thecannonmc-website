<?php

class vote_model extends Model {
	
	public function getTopVoters($limit)
	{
// 		$id = $this->escapeString($id);
		$result = $this->query('SELECT * FROM `votes` ORDER BY `votes` DESC LIMIT ' . $limit);
		return $result;
	}
	
	public function getTotalVotes(){
	    $result = $this->query('SELECT SUM(`votes`) FROM `votes`');
		return $result[0]->{"SUM(`votes`)"};
	}
	
	public function getRecentVoters($limit){
	    $result = $this->query('SELECT * FROM `votes` ORDER BY `last_vote` DESC LIMIT ' . $limit);
	    return $result;
	}
	
	public function setUserName($id, $username){
	    $this->execute('UPDATE votes SET username="' . $username . '" WHERE id=' . $id);
	}
	
	public function dumpUsernameCache(){
	    $this->execute('UPDATE votes SET username=""');
	}
	
	public function incrementUser($uuid){
	    $this->execute('INSERT INTO votes (`uuid`) VALUES ("' . $uuid . '") ON DUPLICATE KEY UPDATE `votes` = votes + 1');
	    file_put_contents(ROOT_DIR . 'files/increments.log', $uuid . "\n", FILE_APPEND);
	}
}

?>
