<?php

define("HOSTNAME", "play.thecannonmc.com");

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TheCannonMC | <?=ucfirst(basename($_SERVER['REQUEST_URI'], ".php"));?></title>

    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      
    <nav class="navbar navbar-inverse">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="portal.php">TheCannonMC</a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="active"><a href="home.php">Home</a></li>
            <li><a target="_blank" href="http://thecannonmc.buycraft.net/">Store</a></li>
            <li><a href="/community">Community</a></li>
            <li><a href="staff.php">Staff</a></li>
            <li><a href="vote.php">Vote</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li><span class="navbar-text"><strong>90</strong> players online!</span></li>
              <li><a href="#" id="ip-tooltip" data-toggle="tooltip" data-placement="bottom" title="Click to copy our IP!" onClick="copy('<?=HOSTNAME;?>');"><?=HOSTNAME;?></a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
    
    <div class="container">
        <div class="row">
            <div class="col-md-6" id="announcement-feed">
                <h2 class="content-lead">Announcements&nbsp;<small class="pull-right"><a href="#"><i class="fa fa-external-link"></i></a></small></h2>
                <?php
                for($i = 0; $i < 5; $i++){
                    echo '
              <div class="announcement" onClick="launchEvent("");">
                <div class="row">
                  <div class="col-md-1">
                    <img class="panel-avatar" src="https://minotar.net/helm/Explodified/50.png">
                  </div>
                  
                  <div class="col-md-11">
                    <div class="panel panel-default">
                      <div class="panel-body announcement-body">
                        <h4>Some new announcement</h4>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                    ';
                }
                ?>
              
            </div>
            <div class="col-md-6" id="twitter-feed">
                <h2 class="content-lead">Twitter Feed</h2>
                
                <div class="twitter-feed">
              		<a href="https://twitter.com/thecannonmc" class="first"> 
              			@TheCannonMC
              		</a>
              		<ul class="timeline">
              			<!--<h3 class="action center"><i class="fa fa-cog fa-spin"></i> Getting the latest data for you...</h3>-->
              			
              			<li>
              				<div class="avatar">
                        <img src="https://pbs.twimg.com/profile_images/604479754838798336/gOc_f0st_400x400.png">
              					<div class="hover">
              						<div class="icon-twitter"></div>
              					</div>
              				</div>
              				<div class="bubble-container">
              					<div class="bubble">
              						<h3>@thecannonmc</h3><br/>
              					Server Beta Whitelist Only! #MinecraftServer
              					</div>
              					
              					<div class="arrow"></div>
              				</div>
              			</li>
              			
              		</ul>
              	</div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h2 class="content-lead">Top Voters | 3,000 Total Votes</h2>
                <?php
                for($i = 1; $i < 6; $i++){
                    echo '
              <div class="announcement" onClick="launchEvent("");">
                <div class="row">
                  <div class="col-md-1">
                    <img class="panel-avatar" src="https://minotar.net/helm/Explodified/50.png">
                  </div>
                  
                  <div class="col-md-11">
                    <div class="panel panel-default">
                      <div class="panel-body announcement-body">
                        <h4>#' . $i. ' GenericPlayerName > 400 votes</h4>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                    ';
                }
                ?>
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="js/app.js"></script>
  </body>
</html>