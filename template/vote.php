<?php

define("HOSTNAME", "play.thecannonmc.com");

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TheCannonMC | <?=ucfirst(basename($_SERVER['REQUEST_URI'], ".php"));?></title>

    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      
    <nav class="navbar navbar-inverse">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="portal.php">TheCannonMC</a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="home.php">Home</a></li>
            <li><a target="_blank" href="http://thecannonmc.buycraft.net/">Store</a></li>
            <li><a href="/community">Community</a></li>
            <li><a href="staff.php">Staff</a></li>
            <li class="active"><a href="vote.php">Vote</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li><span class="navbar-text"><strong>90</strong> players online!</span></li>
              <li><a href="#" id="ip-tooltip" data-toggle="tooltip" data-placement="bottom" title="Click to copy our IP!" onClick="copy('<?=HOSTNAME;?>');"><?=HOSTNAME;?></a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
    
    <div class="container">
        <div class="row">
            <div class="col-md-6" id="announcement-feed">
                <h2 class="content-lead">Top 10 Voters | 300 total votes</h2>
                <?php
                    for($i = 0; $i < 5; $i++){
                        echo '
                        <div class="announcement" onClick="launchEvent("");">
                            <div class="row">
                                <div class="col-md-1">
                                    <img class="panel-avatar" src="https://minotar.net/helm/Explodified/50.png">
                                </div>
                                
                                <div class="col-md-11">
                                    <div class="panel panel-default">
                                        <div class="panel-body announcement-body">
                                            <h4>#' . ( $i + 1 ) . ' Explodified > 250 votes</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        ';
                    }
                ?>
            </div>
            <div class="col-md-6" id="twitter-feed">
                <h2 class="content-lead">Recently Voted</h2>
                <?php
                    for($i = 0; $i < 5; $i++){
                        echo '
                        <div class="announcement" onClick="launchEvent("");">
                            <div class="row">
                                <div class="col-md-1">
                                    <img class="panel-avatar" src="https://minotar.net/helm/Explodified/50.png">
                                </div>
                                
                                <div class="col-md-11">
                                    <div class="panel panel-default">
                                        <div class="panel-body announcement-body">
                                            <h4>Explodified > 250 votes</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        ';
                    }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs vote-tabs" id="tabList">
                    <li role="presentation" class="active"><a href="#1" aria-controls="1">Vote #1</a></li>
                    <li role="presentation"><a href="#2" aria-controls="2">Vote #2</a></li>
                    <li role="presentation"><a href="#3" aria-controls="3">Vote #3</a></li>
                    <li role="presentation"><a href="#4" aria-controls="4">Vote #4</a></li>
                    <li role="presentation"><a href="#5" aria-controls="5">Vote #5</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="1"><iframe src="http://www.ozairpatel.com/" width="100%" height="750px"></iframe></div>
                    <div role="tabpanel" class="tab-pane" id="2"><iframe src="http://www.ozairpatel.com/" width="100%" height="750px"></iframe></div>
                    <div role="tabpanel" class="tab-pane" id="3"><iframe src="http://www.ozairpatel.com/" width="100%" height="750px"></iframe></div>
                    <div role="tabpanel" class="tab-pane" id="4"><iframe src="http://www.ozairpatel.com/" width="100%" height="750px"></iframe></div>
                    <div role="tabpanel" class="tab-pane" id="5"><iframe src="http://www.ozairpatel.com/" width="100%" height="750px"></iframe></div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript">
        $('#tabList a').click(function (e) {
          e.preventDefault()
          $(this).tab('show')
        })
    </script>
  </body>
</html>