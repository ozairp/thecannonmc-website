<?php

define("HOSTNAME", "play.thecannonmc.com");

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TheCannonMC | <?=ucfirst(basename($_SERVER['REQUEST_URI'], ".php"));?></title>

    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="splash">
      
    <nav class="navbar transparent navbar-inverse navbar-fixed-top">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="portal.php">TheCannonMC</a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="home.php">Home</a></li>
            <li><a target="_blank" href="http://thecannonmc.buycraft.net/">Store</a></li>
            <li><a href="/community">Community</a></li>
            <li><a href="staff.php">Staff</a></li>
            <li><a href="vote.php">Vote</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li><span class="navbar-text"><strong>90</strong> players online!</span></li>
              <li><a href="#" id="ip-tooltip" data-toggle="tooltip" data-placement="bottom" title="Click to copy our IP!" onClick="copy('<?=HOSTNAME;?>');"><?=HOSTNAME;?></a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
    
    <div class="landing" id="first">
        <h1>The Next Generation of Minecraft</h1>
        <h2>Some Subtext Here</h2>
        <h3><i class="fa fa-angle-double-down"></i></h3>
    </div>
    <div class="landing" id="second">
        <h1>Join over 300 players</h1>
        <h2>play.thecannonmc.com</h2>
        <h3><i class="fa fa-angle-double-down"></i></h3>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="js/parallax.min.js"></script>
    
    <script type="text/javascript" src="js/app.js"></script>
  </body>
</html>